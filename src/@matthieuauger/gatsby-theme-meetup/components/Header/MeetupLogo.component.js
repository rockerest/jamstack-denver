import React from 'react'
import meetupLogoImg from '../../images/JAMstack_Denver.svg'

const MeetupLogo = () => <img src={meetupLogoImg} alt="JAMstack.paris logo" />

export default MeetupLogo
